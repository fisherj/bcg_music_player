/* ---------------- (c) 2020 James Fisher (james@waters-fisher.id.au) - */ /*!

  \file       ?.?

  \brief

  \details

  \copyright  2020 James Fisher (james@waters-fisher.id.au)

*/ /* --------------------------------------------------------------------- */

import 'package:flutter/material.dart';

import 'package:bcg_music_player/music_player.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'BCG Music Player',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MusicPlayer()
    );
  }
}
/* ---------------- (c) 2020 James Fisher (james@waters-fisher.id.au) - */ /*!

  \file       itunes.dart

  \brief      Handle the results of an iTunes search.

  \details

  \copyright  2020 James Fisher (james@waters-fisher.id.au)

*/ /* --------------------------------------------------------------------- */

/* -------------------------------------------------------------------- */ /*!

  \class    iTunesTrack

  \details  Handle a single track item from iTunes.  There is a lot more
            information available that the application currently does not use.

*/ /* --------------------------------------------------------------------- */

class iTunesTrack {
  bool playing = false;
  bool paused = false;

  String artistName;
  String trackName;
  String albumName;
  String previewUrl;
  String artworkUrl;

  iTunesTrack({
    this.artistName,
    this.trackName,
    this.albumName,
    this.previewUrl,
    this.artworkUrl
  });

  factory iTunesTrack.fromJson(Map<String, dynamic> json) {
    return iTunesTrack(
      artistName: json['artistName'] as String,
      trackName: json['trackName'] as String,
      albumName: json['collectionName'] as String,
      previewUrl: json['previewUrl'] as String,
      artworkUrl: json['artworkUrl60'] as String
    );
  }
}

/* -------------------------------------------------------------------- */ /*!

  \class    iTunesSearchResult

  \details  Holds the results of the iTunes library search.

*/ /* --------------------------------------------------------------------- */

class iTunesSearchResult {
  List<iTunesTrack> tracks;

  iTunesSearchResult({this.tracks});

  factory iTunesSearchResult.fromJson(Map<String, dynamic> json) {
    List<iTunesTrack> tracks = <iTunesTrack>[];

    for(Map track in json['results']) {
      tracks.add(
        new iTunesTrack.fromJson(track)
      );
    }

    return iTunesSearchResult(
      tracks: tracks
    );
  }
}
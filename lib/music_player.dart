/* ---------------- (c) 2020 James Fisher (james@waters-fisher.id.au) - */ /*!

  \file       music_player.dart

  \brief      Handle the main view and controls for the music player.

  \details

  \copyright  2020 James Fisher (james@waters-fisher.id.au)

*/ /* --------------------------------------------------------------------- */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

import 'package:http/http.dart' as http;

import 'package:bcg_music_player/itunes.dart';
import 'package:bcg_music_player/track_player.dart';

/* -------------------------------------------------------------------- */ /*!

  \class    MusicPlayer

  \details

*/ /* --------------------------------------------------------------------- */

class MusicPlayer extends StatefulWidget {
  MusicPlayer({Key key}) : super(key: key);

  @override
  _MusicPlayerState createState() => new _MusicPlayerState();
}

/* -------------------------------------------------------------------- */ /*!

  \class    _MusicPlayerState

  \details

*/ /* --------------------------------------------------------------------- */

class _MusicPlayerState extends State<MusicPlayer>
                        with SingleTickerProviderStateMixin {
  final double CONTROLLER_ICONSIZE = 40.0;

  bool searching = false;
  bool playerController = false;

  TrackPlayer playingTrack;

  iTunesSearchResult tracks;

  AnimationController controller;
  Animation<Offset> offset;

  TextEditingController editingController = TextEditingController();

  /* ------------------------------------------------------------------ */ /*!

    \details      Setup everything we need to allow searching and audio
                  playback.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 4:54:21pm

  */ /* ------------------------------------------------------------------- */

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: 1)
    );

    offset = Tween<Offset>(
      begin: Offset(0.0, 1.0),
      end: Offset.zero
    ).animate(controller);

    playingTrack = null;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Stop an previous playback and start playing a new
                  track if it's not the same as the current track.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:39:39pm

    \param        track               New track to start playing.

    \retval       true if playback started, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> PlaybackStart(iTunesTrack track) async {
    bool success = false;

    iTunesTrack lasttrack = null;

    if(playingTrack != null) {
      lasttrack = playingTrack.track;
    }

    if(track != lasttrack) {
      ShowController();

      PlaybackStop();

      playingTrack = new TrackPlayer(track: track, onStopped: PlaybackStopped);

      if((success = await playingTrack.start()) == true) {
        setState(() {
          playingTrack.paused;
        });
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Callback to handle when the audio playback has stopped.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 9:30:05pm

  */ /* ------------------------------------------------------------------- */

  void PlaybackStopped() {
    setState(() {
      playingTrack.playing;
    });
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Stop the current track playing if there is one.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:39:44pm

    \retval       true if playback stopped, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> PlaybackStop() async {
    bool success = false;

    if(playingTrack != null) {
      if((success = await playingTrack.stop()) == true) {
        setState(() {
          playingTrack.playing;
        });
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Pause playback of the audio track if it's currently playing.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:39:50pm

    \retval       true if playback paused, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> PlaybackPause() async {
    bool success = false;

    if(playingTrack != null) {
      if((success = await playingTrack.pause()) == true) {
        setState(() {
          playingTrack.paused;
        });
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Resume playback of the audio track if it's currently paused.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:39:58pm

    \retval       true if playback resumed, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> PlaybackResume() async {
    bool success = false;

    if(playingTrack != null) {
      if((success = await playingTrack.resume()) == true) {
        setState(() {
          playingTrack.paused;
        });
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!
     
    \details      Show the audio controller on screen.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 8:31:46pm
     
  */ /* ------------------------------------------------------------------- */

  void ShowController() {
    if(playerController == false) {
      SwitchController();

      playerController = true;
    }
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Remove the audio controller from the screen.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 8:32:40pm

  */ /* ------------------------------------------------------------------- */

  void HideController() {
    if(playerController == true) {
      SwitchController();

      playerController = false;
    }
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Switch the controller on or off.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Sunday 11 April 2021 1:03:31pm

  */ /* ------------------------------------------------------------------- */

  void SwitchController() {
    switch(controller.status) {
      case AnimationStatus.completed: {
        controller.reverse();

        break;
      }

      case AnimationStatus.dismissed: {
        controller.forward();

        break;
      }

      default:
    }
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Search for the artist that the user has typed in.  If we
                  are playing any music, leave it playing, but remove the
                  reference to the track data.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 5:27:03pm

    \param        name                Name to search for.

  */ /* ------------------------------------------------------------------- */

  void SearchArtist(String name) async {
    setState(() {
      searching = true;
    });

    if(playingTrack != null) {
      if(playingTrack.playing == true) {
        playingTrack.track = null;
      }
    }

    var query = {
      'term': '$name',
      'country': 'AU',
      'media': 'music',
      'explicit': 'No'
    };

    var response = await http.get(
      Uri.https('itunes.apple.com', 'search', query)
    );

    if(response.statusCode != 200) {
      throw Exception('Server error');
    }

    var data = json.decode(response.body);

    tracks = iTunesSearchResult.fromJson(data);

    setState(() {
      searching = false;
    });
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Build the main screen display.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 5:02:12pm

    \retval       Widget containing the screen layout.

  */ /* ------------------------------------------------------------------- */

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          SafeArea(
            child: Column(
              children: <Widget>[
                SearchBox(),
                Container(
                  child: TrackView()
                )
              ],
            ),
          ),
          AudioControls(),
        ]
      )
    );
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Return the artist search box.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 4:59:45pm

    \retval       Widget containing the search box.

  */ /* ------------------------------------------------------------------- */

  Widget SearchBox() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        controller: editingController,
        onSubmitted: (value) {
          SearchArtist(value);
        },
        decoration: InputDecoration(
          labelText: "Search artist",
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(4.0)
            )
          )
        ),
      ),
    );
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Create the track view. This can include the progress
                  indicator, the track list or nothing.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 8:36:24pm

    \retval       Widget required.

  */ /* ------------------------------------------------------------------- */

  Widget TrackView() {
    if(searching == true) {
      return CircularProgressIndicator();
    }

    if(tracks != null) {
      return Expanded(
        child: ListView.separated(
          shrinkWrap: true,
          itemCount: tracks.tracks.length,
          itemBuilder: (context, index) {
            return TrackWidget(tracks.tracks[index]);
          },
          separatorBuilder: (context, index) {
            return Divider(
              height: 4.0,
              indent: 16.0,
              endIndent: 16.0,
            );
          },
        ),
      );
    }

    return Text('');
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Display all the required track information.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Sunday 11 April 2021 12:51:57pm

    \param        track               iTunes track information.

    \retval       Widget containing the track view.

  */ /* ------------------------------------------------------------------- */

  Widget TrackWidget(iTunesTrack track) {
    return GestureDetector(
      onTap: () {
        PlaybackStart(track);
      },
      child:  Container(
        height: 60.0 + 4.0 + 4.0,
        color: Colors.white,
        child: Row(
          children: <Widget>[
            SizedBox(
              width: 16.0
            ),
            ClipRRect(
              borderRadius: BorderRadius.circular(4.0),
              child: Image.network(
                track.artworkUrl
              ),
            ),
            SizedBox(
              width: 8.0
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${track.trackName}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 14.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '${track.artistName}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12.0,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    '${track.albumName}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 8.0,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: TrackPlaying(track)
            ),
            SizedBox(
              width: 16.0
            ),
          ],
        ),
      )
    );
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Display the current playing status of the current track.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 9:20:13pm

    \param        track               iTunes track information.

    \retval       Widget containing the status.

  */ /* ------------------------------------------------------------------- */

  Widget TrackPlaying(iTunesTrack track) {
    if(playingTrack != null) {
      if(playingTrack.track == track) {
        if(playingTrack.playing == true) {
          if(playingTrack.paused == true) {
            return Icon(
              Icons.bar_chart,
              size: 40.0,
              color: Colors.black12
            );
          } else {
            return Icon(
              Icons.bar_chart,
              size: 40.0,
              color: Colors.black
            );
          }
        }
      }
    }

    return Text('');
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Return the audio controls widget - this is shown or hidden
                  from the bottom of the screen as required.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 4:55:19pm

    \retval       Widget containing the controls.

  */ /* ------------------------------------------------------------------- */

  Widget AudioControls() {
    return Align(
      alignment: Alignment.bottomCenter,
      child: SlideTransition(
        position: offset,
        child: Card(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Icon(
                  Icons.skip_previous,
                  size: CONTROLLER_ICONSIZE,
                  color: Colors.black12
                )
              ),
              Expanded(
                child: Icon(
                  Icons.fast_rewind,
                  size: CONTROLLER_ICONSIZE,
                  color: Colors.black12
                )
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if(playingTrack != null) {
                      if(playingTrack.playing == true) {
                        PlaybackStop();
                      }
                    }
                  },
                  child: StopIcon()
                )
              ),
              Expanded(
                child: GestureDetector(
                  onTap: () {
                    if(playingTrack != null) {
                      if(playingTrack.paused == true) {
                        PlaybackResume();
                      } else {
                        PlaybackPause();
                      }
                    }
                  },
                  child: PlayPauseIcon()
                )
              ),
              Expanded(
                child: Icon(
                  Icons.fast_forward,
                  size:CONTROLLER_ICONSIZE,
                  color: Colors.black12
                )
              ),
              Expanded(
                child: Icon(
                  Icons.skip_next,
                  size: CONTROLLER_ICONSIZE,
                  color: Colors.black12
                )
              ),
            ]
          )
        ),
      ),
    );
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Generate the correct stop icon.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Sunday 11 April 2021 11:44:18am

    \retval       Widget withe the correct icon.

  */ /* ------------------------------------------------------------------- */

  Widget StopIcon() {
    Color stop = Colors.black12;

    if(playingTrack != null) {
      if(playingTrack.playing == true) {
        stop = Colors.black;
      }
    }

    return Icon(
      Icons.stop,
      size: CONTROLLER_ICONSIZE,
      color: stop
    );
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Generate the correct play/pause icon.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 8:58:16pm

    \retval       Widget withe the correct icon.

  */ /* ------------------------------------------------------------------- */

  Widget PlayPauseIcon() {
    IconData icon = Icons.play_arrow;
    Color color = Colors.black12;

    if(playingTrack != null) {
      if(playingTrack.playing == true) {
        color = Colors.black;

        if(playingTrack.paused == false) {
          icon = Icons.pause;
        }
      }
    }

    return Icon(
      icon,
      size: CONTROLLER_ICONSIZE,
      color: color
    );
  }
}
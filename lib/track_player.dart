/* ---------------- (c) 2020 James Fisher (james@waters-fisher.id.au) - */ /*!

  \file       track_player.dart

  \brief      Control the audio functions of a track.

  \details

  \copyright  2020 James Fisher (james@waters-fisher.id.au)

*/ /* --------------------------------------------------------------------- */

import 'package:flutter/foundation.dart';

import 'package:audioplayers/audioplayers.dart';

import 'package:bcg_music_player/itunes.dart';

/* -------------------------------------------------------------------- */ /*!

  \class    TrackPlayer

  \details

*/ /* --------------------------------------------------------------------- */

class TrackPlayer {
  bool _playing;
  bool _paused;

  iTunesTrack _track;

  AudioPlayer _player;

  VoidCallback _onStopped;

  /* ------------------------------------------------------------------ */ /*!

    \details      Class constructor.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:29:34pm

    \param        track               Reference to iTunes track information.

  */ /* ------------------------------------------------------------------- */

  TrackPlayer({@required track, @required onStopped}) : _track = track, _onStopped = onStopped {
    _playing = false;
    _paused = false;

    _player = AudioPlayer();

    _player.onPlayerCompletion.listen((event) {
      stop();
    });
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Class destructor as a function as Dart does not support one.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:26:56pm

  */ /* ------------------------------------------------------------------- */

  Future<void> destructor() async {
    await stop();

    _track = null;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Getters.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:38:26pm

  */ /* ------------------------------------------------------------------- */

  bool get playing => _playing;
  
  bool get paused => _paused;

  iTunesTrack get track => _track;

  set track(iTunesTrack track) => _track = track;
  
  /* ------------------------------------------------------------------ */ /*!

    \details      Start playback of the preview audio track if one has been
                  specified in the iTunes data.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:28:02pm

    \retval       true if playback started, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> start() async {
    bool success = false;

    if(_playing == false) {
      if(_track != null) {
        if(_track.previewUrl != null) {
          if(await _player.play(_track.previewUrl) == 1) {
            success = true;

            _paused = false;
            _playing = true;
          }
        }
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Stop playback of the audio track if it's currently playing.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:32:01pm

    \retval       true if playback stopped, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> stop() async {
    bool success = false;

    if(_playing == true) {
      if(await _player.stop() == 1) {
        success = true;

        _playing = false;

        _onStopped();
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Pause playback of the audio track if it's currently playing.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:33:17pm

    \retval       true if playback paused, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> pause() async {
    bool success = false;

    if(_playing == true) {
      if(_paused == false) {
        if(await _player.pause() == 1) {
          success = true;

          _paused = true;
        }
      }
    }

    return success;
  }

  /* ------------------------------------------------------------------ */ /*!

    \details      Resume playback of the audio track if it's currently paused.

    \author       James Fisher (james@waters-fisher.id.au)
    \date         Saturday 10 April 2021 6:33:52pm

    \retval       true if playback resumed, false if there was a problem.

  */ /* ------------------------------------------------------------------- */

  Future<bool> resume() async {
    bool success = false;

    if(_playing == true) {
      if(_paused == true) {
        if(await _player.resume() == 1) {
          success = true;

          _paused = false;
        }
      }
    }

    return success;
  }
}
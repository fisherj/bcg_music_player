# bcg_music_player

BCG Music Player

## Getting Started

This flutter application was delevoped by James Fisher (james@waters-fisher.id.au).

To use this repository, the following tools are required:

- Android Studio 4.1.3
 - Build #AI-201.8743.12.41.7199119, built on March 11, 2021
 - Runtime version: 1.8.0_242-release-1644-b3-6915495 x86_64
 - VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o
 - macOS 10.16
 - Cores: 8
- Flutter 2.0.3 • channel stable • https://github.com/flutter/flutter.git
 - Framework • revision 4d7946a68d (3 weeks ago) • 2021-03-18 17:24:33 -0700
 - Engine • revision 3459eb2436
 - Tools • Dart 2.12.2

The development was done using a Pixel 2 emulator running API 29 and also tested
on a Nokia 7.2 running Android 10.  Once the project has been opened using Android
Studio, open the 'pubspec.yaml' file and click 'Pub Get' at the top of the
window. This will ensure the 2 libraries used (http & audioplayers) are loaded. To
run the debug version, run the emulator via Tools->AVD Manager, select the 'Android
SDK 29' and then select Run->Debug 'main.dart'.

All functionality was added including playback in the background when a new search
list was generated.